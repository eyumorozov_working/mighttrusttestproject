﻿using Microsoft.Practices.Unity;
using MightTrust.Client.ViewModel;
using MightTrust.Model;
using MightTrust.Model.WebInteraction;
using NLog;
using System;
using System.Configuration;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Threading;

namespace MightTrust.Client
{
    public partial class App : Application
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            using (var container = new UnityContainer())
            {
                container.RegisterInstance("ServiceAddress", ConfigurationManager.AppSettings["ServiceAddress"]);
                var conn = ConfigurationManager.ConnectionStrings["SQLiteConnection"].ConnectionString;
                container.RegisterType<Func<SQLiteConnection>>(new InjectionFactory(_ => new Func<SQLiteConnection>(() => 
                    {
                        var connectionString = ConfigurationManager.ConnectionStrings["SQLiteConnection"].ConnectionString;
                        var connection = new SQLiteConnection(connectionString);
                        connection.Open();
                        return connection;
                    })));
                container.RegisterInstance(Dispatcher.CurrentDispatcher);
                container.RegisterType<IWebClient, WebClient>();
                container.RegisterInstance(typeof(ISettingsRepository), container.Resolve<SettingsRepository>());
                var searchVm = container.Resolve<SettingsVm>();
                var mainWindow = container.Resolve<MainWindow>();
                mainWindow.DataContext = searchVm;
                mainWindow.Show();
            }
        }
    }
}