﻿using Microsoft.Practices.Unity;
using MightTrust.Client.Common;
using MightTrust.Model;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;

namespace MightTrust.Client.ViewModel
{
    public class SettingsVm : INotifyPropertyChanged
    {
        private ObservableCollection<Setting> _results;
        private string _fieldToSearch;
        private bool _isLoading;
        private ISettingsRepository _settingsRepositoru;

        public SettingsVm()
        {
            SendMessage = new RelayCommand(DoSendMessage);
        }

        [Dependency("ServiceAddress")]
        public string ServiceAddress { get; set; }

        [Dependency]
        public Dispatcher Dispatcher { get; set; }

        [Dependency]
        public ISettingsRepository SettingsRepository
        {
            get { return _settingsRepositoru; }
            set
            {
                _settingsRepositoru = value;
                LoadLastRequested();
            }
        }

        public RelayCommand SendMessage { get; private set; }

        public string FieldToSearch
        {
            get { return _fieldToSearch; }
            set
            {
                if (_fieldToSearch == value)
                    return;
                _fieldToSearch = value;
                OnPropertyChanged("FieldToSearch");
                SendMessage.CanExecute = !IsLoading && !string.IsNullOrWhiteSpace(_fieldToSearch);
            }
        }

        public ObservableCollection<Setting> Results
        {
            get { return _results; }
            set
            {
                if (Equals(_results, value))
                    return;
                _results = value;
                OnPropertyChanged("Results");
            }
        }

        public bool IsLoading
        {
            get { return _isLoading; }
            private set
            {
                if (_isLoading == value)
                    return;
                _isLoading = value;
                OnPropertyChanged("IsLoading");
                SendMessage.CanExecute = !_isLoading && !string.IsNullOrWhiteSpace(FieldToSearch);
            }
        }

        private void DoSendMessage()
        {
            IsLoading = true;
            ThreadPool.QueueUserWorkItem(_ => 
                {
                    var serviceSettings = SettingsRepository.GetFromService(ServiceAddress + FieldToSearch);
                    Dispatcher.SyncRun(() => 
                        {
                            Results.AddRange(serviceSettings);
                            FieldToSearch = "";
                            IsLoading = false;
                        });
                });
        }

        private void LoadLastRequested()
        {
            IsLoading = true;
            ThreadPool.QueueUserWorkItem(_ =>
                {
                    var lastRequested = SettingsRepository.GetLastRequestedFields();
                    Dispatcher.SyncRun(() =>
                        {
                            Results = new ObservableCollection<Setting>(lastRequested);
                            IsLoading = false;
                        });
                });
        }

        #region INotifyPropertyChanged members

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged = null;

        #endregion INotifyPropertyChanged members
    }
}