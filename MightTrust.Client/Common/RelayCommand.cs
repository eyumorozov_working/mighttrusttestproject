﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace MightTrust.Client.Common
{
    public class RelayCommand : ICommand, INotifyPropertyChanged
    {
        private bool _canExecute;
        private readonly Action<object> _action;

        public RelayCommand(Action<object> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            _action = action;
        }

        public RelayCommand(Action action):this(_ => action()) { }

        public bool CanExecute
        {
            get { return _canExecute; }
            set
            {
                if (_canExecute == value)
                    return;
                _canExecute = value;
                OnCanExecuteChanged();
                OnPropertyChanged("CanExecute");
            }
        }

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute;
        }

        public void Execute(object parameter)
        {
            _action(parameter);
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CanExecuteChanged = null;
        public event PropertyChangedEventHandler PropertyChanged = null;
    }
}
