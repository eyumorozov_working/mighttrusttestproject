﻿using System;
using System.Windows.Threading;

namespace MightTrust.Client.Common
{
    public static class DispatcherHelpers
    {
        public static void SyncRun(this Dispatcher dispatcher, Action action)
        {
            if (!dispatcher.CheckAccess())
                dispatcher.BeginInvoke(action);
            else
                action();
        }
    }
}
