﻿using Newtonsoft.Json;

namespace MightTrust.Model
{
    public class Setting
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}