﻿using System.Collections.Generic;

namespace MightTrust.Model
{
    public interface ISettingsRepository
    {
        IEnumerable<Setting> GetLastRequestedFields();
        IEnumerable<Setting> GetFromService(string args);
    }
}