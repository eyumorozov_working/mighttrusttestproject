﻿namespace MightTrust.Model.WebInteraction
{
    public interface IWebClient
    {
        IWebClientResponse Get(string url);
    }
}