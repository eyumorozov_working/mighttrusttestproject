﻿using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace MightTrust.Model.WebInteraction
{
    public class WebClient : IWebClient
    {
        public IWebClientResponse Get(string url)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            ServicePointManager.ServerCertificateValidationCallback = ValidateServerSertificate;
            var response = (HttpWebResponse)request.GetResponse();
            return new WebClientResponse(response);
        }

        private bool ValidateServerSertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) => true;
    }
}