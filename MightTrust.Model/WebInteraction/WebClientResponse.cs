﻿using System;
using System.IO;
using System.Net;

namespace MightTrust.Model.WebInteraction
{
    public class WebClientResponse : IWebClientResponse
    {
        private HttpWebResponse _response;

        public WebClientResponse(HttpWebResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response");
            _response = response;
        }

        public HttpStatusCode StatusCode => _response.StatusCode;

        public string GetResponseString()
        {
            string result;
            var responseStream = _response.GetResponseStream();
            if (responseStream == null)
                return "";
            using (var reader = new StreamReader(responseStream))
            {
                result = reader.ReadToEnd();
            }
            return result;
        }
    }
}