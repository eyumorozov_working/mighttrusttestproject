﻿using System.Net;

namespace MightTrust.Model.WebInteraction
{
    public interface IWebClientResponse
    {
        HttpStatusCode StatusCode { get; }
        string GetResponseString();
    }
}
