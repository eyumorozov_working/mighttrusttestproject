﻿using Microsoft.Practices.Unity;
using MightTrust.Model.WebInteraction;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;

namespace MightTrust.Model
{
    public class SettingsRepository : ISettingsRepository
    {
        private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
        private Func<SQLiteConnection> _connectionFactory;
        private readonly object _locker = new object();

        [Dependency]
        public IWebClient WebClient { get; set; }

        [Dependency]
        public Func<SQLiteConnection> ConnectionFactory
        {
            get { return _connectionFactory; }
            set
            {
                if (Equals(_connectionFactory, value))
                    return;
                _connectionFactory = value;
                CreateTableIfDoesntExists();
            }
        }

        private void CreateTableIfDoesntExists()
        {
            lock (_locker)
            {
                using (var connection = ConnectionFactory())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = @"CREATE TABLE IF NOT EXISTS Settings(Id INTEGER PRIMARY KEY AUTOINCREMENT, Key TEXT, Value TEXT, IsLastRequested INTEGER)";
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public IEnumerable<Setting> GetFromService(string args)
        {
            var webClientResponse = WebClient.Get(args);
            var responseString = webClientResponse.GetResponseString();
            try
            {
                var settings = JsonConvert.DeserializeObject<IEnumerable<Setting>>(responseString);
                SaveToDb(settings);
                return settings;
            }
            catch(Exception e)
            {
                _logger.Error(e, e.Message);
                return Enumerable.Empty<Setting>();
            }
        }

        private void SaveToDb(IEnumerable<Setting> settings)
        {
            lock (_locker)
            {
                using (var connection = ConnectionFactory())
                {
                    using (var updateCommand = connection.CreateCommand())
                    {
                        updateCommand.CommandText = @"UPDATE Settings SET IsLastRequested = 0 WHERE IsLastRequested = 1";
                        updateCommand.ExecuteNonQuery();
                    }

                    using (var insertCommand = connection.CreateCommand())
                    {
                        foreach (var setting in settings)
                        {
                            insertCommand.CommandText = @"INSERT INTO Settings(Key, Value, IsLastRequested) VALUES(@key, @value, 1)";
                            insertCommand.Parameters.AddWithValue("key", setting.Key);
                            insertCommand.Parameters.AddWithValue("value", setting.Value);
                            insertCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
        }

        public IEnumerable<Setting> GetLastRequestedFields()
        {
            var result = new List<Setting>();
            using (var connection = ConnectionFactory())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = @"SELECT Id, Key, Value FROM Settings WHERE IsLastRequested = 1";
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var setting = new Setting
                            {
                                Id = reader.GetInt32(reader.GetOrdinal("Id")),
                                Key = reader.GetString(reader.GetOrdinal("Key")),
                                Value = reader.GetString(reader.GetOrdinal("Value"))
                            };
                            result.Add(setting);
                        }
                    }
                }
            }
            return result;
        }
    }
}