﻿using MightTrust.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace MightTrust.Service.Controllers
{
    public class SettingsController: ApiController
    {
        public JsonResult<object> GetSettings(string searchField = null)
        {
            if (string.IsNullOrWhiteSpace(searchField))
                return Json(new object());
            var strings = searchField.Split(' ');
            var settings = new List<Setting>();
            foreach (var str in strings)
                settings.Add(new Setting { Key = str, Value = "value for " + str });
            var result = JsonConvert.SerializeObject(settings);
            return Json((object)settings);
        }
    }
}